﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Przedprzeplywy
{
    class Program
    {



        // Driver program to test above functions
        static void Main(string[] args)
        {

            PushRelabel pushRelabel = new PushRelabel(exampleGraph());
            //PushRelabel pushRelabel = exampleGraph2();

            // Set source and sink
            int source = 0;
            int sink = 5;

            // run algorithm
            int flow = pushRelabel.getMaxFlow(source, sink);

            pushRelabel.showFlowGraph();

            Console.WriteLine("\n\nMaximum flow: " + flow);
            Console.ReadKey();
        }

        private static int[][] exampleGraph()
        {
            int size = 6;

            int[][] graph = new int[size][];

            for (int i = 0; i < size; i++)
            {
                graph[i] = new int[size];
                for (int j = 0; j < size; j++)
                    graph[i][j] = 0;
            }

            graph[0][1] = 15;
            graph[0][2] = 4;

            graph[1][3] = 12;

            graph[2][4] = 10;

            graph[3][2] = 3;
            graph[3][5] = 7;

            graph[4][1] = 5;
            graph[4][5] = 10;

            return graph;
        }

        private static PushRelabel exampleGraph2()
        {
            PushRelabel pushRelabel = new PushRelabel(6);

            pushRelabel.addEdge(0, 1, 16);
            pushRelabel.addEdge(0, 2, 13);
            pushRelabel.addEdge(1, 2, 10);
            pushRelabel.addEdge(2, 1, 4);
            pushRelabel.addEdge(1, 3, 12);
            pushRelabel.addEdge(2, 4, 14);
            pushRelabel.addEdge(3, 2, 9);
            pushRelabel.addEdge(3, 5, 20);
            pushRelabel.addEdge(4, 3, 7);
            pushRelabel.addEdge(4, 5, 4);

            return pushRelabel;
        }
    }
}