﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Przedprzeplywy
{
    public class PushRelabel
    {
        class Edge
        {
            public int excess_flow;
            public int value;

            public int node1;
            public int node2;

            public Edge(int excess_flow, int value, int node1, int node2)
            {
                this.excess_flow = excess_flow;
                this.value = value;
                this.node1 = node1;
                this.node2 = node2;
            }
        }

        class Node
        {
            public int height;
            public int flow;

            public Node(int h, int flow)
            {
                this.height = h;
                this.flow = flow;
            }
        }


        int SIZE; //size of graph
        List<Node> node = new List<Node>();
        List<Edge> edge = new List<Edge>();

        List<Edge> edge_org;

        //CONSTRUCTORS
        public PushRelabel(int V)  // Constructor w/o graph
        {
            this.SIZE = V;

            // all vertices are initialized with 0 height
            // and 0 excess flow
            for (int i = 0; i < V; i++)
                node.Add(new Node(0, 0));
        }

        public PushRelabel(int[][] G)  // Constructor with graph matrix
        {
            this.SIZE = G.Length;

            for (int i = 0; i < SIZE; i++)
                for (int j = 0; j < SIZE; j++)
                    if (G[i][j] > 0)
                        this.addEdge(i, j, G[i][j]);

            // all vertices are initialized with 0 height
            // and 0 excess flow
            for (int i = 0; i < SIZE; i++)
                node.Add(new Node(0, 0));

        }

        // function to add an edge to graph
        public void addEdge(int u, int v, int value)
        {
            // flow is initialized with 0 for all edge
            edge.Add(new Edge(0, value, u, v));
        }


        // This function is called to initialize flows
        void init(int s)
        {
            edge_org = new List<Edge>(edge);

            // height of source is equal to number of nodes
            node[s].height = node.Count;

            for (int i = 0; i < edge.Count; i++)
            {
                // If edge goes from source
                if (edge[i].node1 == s)
                {
                    // Setting flow equal to capacity
                    edge[i].excess_flow = edge[i].value;

                    // Addint flow to target node
                    node[edge[i].node2].flow += edge[i].excess_flow;

                    // Add an edge to residual graph with 0 capacity
                    edge.Add(new Edge(-edge[i].excess_flow, 0, edge[i].node2, s));
                }
            }
        }


        // push if possible, and return result
        bool push(int u)
        {
            bool possible = false;
            // looking for node from u to which flow can be pushed
            for (int i = 0; i < edge.Count; i++)
            {
                if (edge[i].node1 == u)
                {
                    // if flow is NOT equal to capacity
                    if (!(edge[i].excess_flow == edge[i].value))
                    {
                        // Push if height of target node is smaller than height of overflowing node
                        if (node[u].height > node[edge[i].node2].height)
                        {
                            // Flow to be pushed
                            int flow = Math.Min(edge[i].value - edge[i].excess_flow, node[u].flow); // minimum of remaining flow on edge and excess flow.

                            // Decrease excess flow of overflowing node
                            node[u].flow -= flow;

                            // Increase excess flow of target node
                            node[edge[i].node2].flow += flow;

                            // Add residual flow (With capacity 0 and negative flow)
                            edge[i].excess_flow += flow;
                            updateResidual(i, flow);

                            possible = true;
                        }
                    }                  
                }
            }
            return possible;
        }

        // relabel if flow wash pushed
        void relabel(int u)
        {
            // maximum height (initial value)
            int max = int.MaxValue;

            // Find the node with minimum height
            for (int i = 0; i < edge.Count; i++)
            {
                if (edge[i].node1 == u)
                {
                    // if flow is NOT equal to capacity
                    if (!(edge[i].excess_flow == edge[i].value))
                    {
                        // Update minimum height
                        if (node[edge[i].node2].height < max)
                        {
                            max = node[edge[i].node2].height;

                            // updating height of u
                            node[u].height = max + 1;
                        }
                    }

                }
            }
        }


        // update residual graph
        void updateResidual(int i, int flow)
        {
            int u = edge[i].node2, v = edge[i].node1;

            for (int j = 0; j < edge.Count; j++)
            {
                if (edge[j].node2 == v && edge[j].node1 == u)
                {
                    edge[j].excess_flow -= flow;
                    return;
                }
            }

            // adding reverse Edge in residual graph
            Edge e = new Edge(0, flow, u, v);
            edge.Add(e);
        }

        // returns index of overflowing Vertex
        int getOverflowingNode(List<Node> node)
        {
            for (int i = 1; i < node.Count - 1; i++)
                if (node[i].flow > 0)
                    return i;

            return -1; // -1 if none
        }


        // get maximum flow from s to t
        public int getMaxFlow(int s, int t)
        {
            init(s);

            // loop untill none of the node is overflowing
            while (getOverflowingNode(node) != -1)
            {
                int u = getOverflowingNode(node); //get index of overflowing node

                if (!push(u))
                    relabel(u);
            }

            // returns last node, which is maximum flow
            return node[node.Count - 1].flow;

        }

        public void showFlowGraph()
        {
            Console.WriteLine("Graph:");

            foreach (var e in edge_org)
                    Console.WriteLine(e.node1 + " --(" + +e.value + ")--> " + e.node2);

            Console.WriteLine("\n\nFlow:");

            for(int i=0; i< edge_org.Count; i++)
            {
                if (edge[i].excess_flow >= 0)
                    Console.WriteLine(edge[i].node1 + " --(" + +edge[i].excess_flow + "/" + edge_org[i].value + ")--> " + edge[i].node2);

            }

        }
    }

}
